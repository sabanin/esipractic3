package org.test.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("test")
public class DemoController {
	
	@RequestMapping("en")
	public String text(){
		return "index";
	}
}
